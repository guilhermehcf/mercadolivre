package br.com.mercadolivreteste.rest;

import java.util.ArrayList;

import br.com.mercadolivreteste.BuildConfig;
import br.com.mercadolivreteste.model.CardIssuers;
import br.com.mercadolivreteste.model.Installments;
import br.com.mercadolivreteste.model.PaymentMethods;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiClientInterface {

    @GET(BuildConfig.BASE_URL + "/payment_methods")
    Call<ArrayList<PaymentMethods>> getPaymentMethods(@Query("public_key") String publicKey);

    @GET(BuildConfig.BASE_URL + "/payment_methods/card_issuers")
    Call<ArrayList<CardIssuers>> getCardIssuers(@Query("public_key") String publicKey, @Query("payment_method_id") String paymentMethodId);

    @GET(BuildConfig.BASE_URL + "/payment_methods/installments")
    Call<ArrayList<Installments>> getInstallments(@Query("public_key") String publicKey, @Query("payment_method_id") String paymentMethodId, @Query("amount") double amount, @Query("issuer.id") long issuerId);
}