package br.com.mercadolivreteste;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;

import br.com.mercadolivreteste.model.CardIssuers;
import br.com.mercadolivreteste.model.Installments;
import br.com.mercadolivreteste.model.PaymentMethods;
import br.com.mercadolivreteste.rest.APIClient;
import br.com.mercadolivreteste.rest.ApiClientInterface;
import br.com.mercadolivreteste.ui.paymentMethods.PaymentMethodsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.editTextValor)
    EditText editTextValor;

    public static String CARD_ISSUE_ID = "CARD_ISSUE_ID";
    public static String PAYMENT_TYPE_ID = "PAYMENT_TYPE_ID";
    public static String VALUE_PAYMENT = "VALUE_PAYMENT";
    public static String MESSAGE = "MESSAGE";

    private ApiClientInterface apiService = APIClient.getClient().create(ApiClientInterface.class);
    private ArrayList<PaymentMethods> paymentMethodsList = new ArrayList<PaymentMethods>();
    private ArrayList<CardIssuers> cardIssuersList = new ArrayList<CardIssuers>();
    private ArrayList<Installments> installmentsList = new ArrayList<Installments>();

    private double mAmount = 150;
    private long mIssuerId = 0;
    private String mPaymentMethodId = "";
    private String mMessage = "";
    private long valuePayment = 0;
    private long valuePaymentMessage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        if (editTextValor.getText() != null && !editTextValor.getText().toString().isEmpty()) {
            valuePayment = Long.parseLong(editTextValor.getText().toString());
        }

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            valuePaymentMessage = extras.getLong(VALUE_PAYMENT);
            mPaymentMethodId = extras.getString(PAYMENT_TYPE_ID);
            mIssuerId = extras.getLong(CARD_ISSUE_ID);
            mMessage = extras.getString(MESSAGE);
            if (mPaymentMethodId != null && mIssuerId != 0 && valuePaymentMessage > 0 && mMessage != null) {
                showDialog(mPaymentMethodId, valuePaymentMessage, mIssuerId, mMessage);
            }
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editTextValor.getText() != null && !editTextValor.getText().toString().isEmpty()) {
                    valuePayment = Long.parseLong(editTextValor.getText().toString());
                }
                if (valuePayment > 0) {
                    Intent intent = new Intent(MainActivity.this, PaymentMethodsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(PaymentMethodsActivity.VALUE_PAYMENT, valuePayment);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private void loadPaymentMethods() {

        Call<ArrayList<PaymentMethods>> call = apiService.getPaymentMethods(BuildConfig.PUBLIC_KEY);

        call.enqueue(new Callback<ArrayList<PaymentMethods>>() {
            @Override
            public void onResponse(Call<ArrayList<PaymentMethods>> call, Response<ArrayList<PaymentMethods>> response) {
                paymentMethodsList = response.body();
                if (paymentMethodsList != null && paymentMethodsList.size() > 0) {
                    for (int i = 0; i < paymentMethodsList.size(); i++) {
                        Log.v("MercadoLivre", "PaymentMethodsList Name: " + paymentMethodsList.get(i).getName());
                        if (i == 1) {
                            mPaymentMethodId = paymentMethodsList.get(i).getId();
                        }
                        loadCardIssuers();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PaymentMethods>> call, Throwable t) {
            }
        });
    }

    private void loadCardIssuers() {

        Call<ArrayList<CardIssuers>> call = apiService.getCardIssuers(BuildConfig.PUBLIC_KEY, mPaymentMethodId);

        call.enqueue(new Callback<ArrayList<CardIssuers>>() {
            @Override
            public void onResponse(Call<ArrayList<CardIssuers>> call, Response<ArrayList<CardIssuers>> response) {
                cardIssuersList = response.body();
                if (cardIssuersList != null && cardIssuersList.size() > 0) {
                    for (int i = 0; i < cardIssuersList.size(); i++) {
                        Log.v("MercadoLivre", "CardIssuersList Name: " + cardIssuersList.get(i).getName());
                        if (i == 1) {
                            mIssuerId = Long.parseLong(cardIssuersList.get(i).getId());
                        }
                    }
                    loadInstallments();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CardIssuers>> call, Throwable t) {
            }
        });
    }

    private void loadInstallments() {

        Call<ArrayList<Installments>> call = apiService.getInstallments(BuildConfig.PUBLIC_KEY, mPaymentMethodId, mAmount, mIssuerId);

        call.enqueue(new Callback<ArrayList<Installments>>() {
            @Override
            public void onResponse(Call<ArrayList<Installments>> call, Response<ArrayList<Installments>> response) {
                installmentsList = response.body();
                if (installmentsList != null && installmentsList.size() > 0) {
                    for (int i = 0; i < installmentsList.size(); i++) {
                        for (int y = 0; y < installmentsList.get(i).getPayerCosts().size(); y++) {
                            Log.v("MercadoLivre", "InstallmentsList Message: " + installmentsList.get(i).getPayerCosts().get(y).getRecommended_message());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Installments>> call, Throwable t) {
            }
        });
    }

    private void showDialog(String paymentMethodId, double amount, long issuerId, String message) {
        final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Informações da Compra");
        alertDialog.setMessage("\nMeio de Pagamento: " + paymentMethodId + "\n\nValor: " + amount + "\n\nParcelas de Pagamento: " + message);
        alertDialog.setButton(Dialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}