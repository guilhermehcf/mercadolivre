package br.com.mercadolivreteste.model;

import com.google.gson.annotations.SerializedName;

public class PaymentMethods {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("payment_type_id")
    private String payment_type_id;

    @SerializedName("thumbnail")
    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(String payment_type_id) {
        this.payment_type_id = payment_type_id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}