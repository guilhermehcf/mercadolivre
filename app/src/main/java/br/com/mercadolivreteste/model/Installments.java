package br.com.mercadolivreteste.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Installments {

    @SerializedName("payment_method_id")
    private String payment_method_id;

    @SerializedName("payment_type_id")
    private String payment_type_id;

    @SerializedName("payer_costs")
    private ArrayList<PayerCosts> payerCosts;

    public ArrayList<PayerCosts> getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(ArrayList<PayerCosts> payerCosts) {
        this.payerCosts = payerCosts;
    }

    public String getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(String payment_type_id) {
        this.payment_type_id = payment_type_id;
    }
}