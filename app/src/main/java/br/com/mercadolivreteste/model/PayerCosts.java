package br.com.mercadolivreteste.model;

import com.google.gson.annotations.SerializedName;

public class PayerCosts {

    @SerializedName("recommended_message")
    private String recommended_message;

    public String getRecommended_message() {
        return recommended_message;
    }

    public void setRecommended_message(String recommended_message) {
        this.recommended_message = recommended_message;
    }
}
