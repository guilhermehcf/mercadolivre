package br.com.mercadolivreteste.ui.paymentMethods;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import br.com.mercadolivreteste.BuildConfig;
import br.com.mercadolivreteste.R;
import br.com.mercadolivreteste.model.PaymentMethods;
import br.com.mercadolivreteste.rest.APIClient;
import br.com.mercadolivreteste.rest.ApiClientInterface;
import br.com.mercadolivreteste.ui.cardIssuers.CardIssuersActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethodsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public static String VALUE_PAYMENT = "VALUE_PAYMENT";

    private ApiClientInterface apiService = APIClient.getClient().create(ApiClientInterface.class);
    private ArrayList<PaymentMethods> paymentMethodsList = new ArrayList<PaymentMethods>();
    private String mPaymentMethodId = "";
    private long valuePayment = 0;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_methods);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            valuePayment = extras.getLong(VALUE_PAYMENT);
        }

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        loadPaymentMethods();
    }

    private void loadPaymentMethods() {

        Call<ArrayList<PaymentMethods>> call = apiService.getPaymentMethods(BuildConfig.PUBLIC_KEY);

        call.enqueue(new Callback<ArrayList<PaymentMethods>>() {
            @Override
            public void onResponse(Call<ArrayList<PaymentMethods>> call, Response<ArrayList<PaymentMethods>> response) {
                paymentMethodsList = response.body();
                if (paymentMethodsList != null && paymentMethodsList.size() > 0) {
                    for (int i = 0; i < paymentMethodsList.size(); i++) {
                        Log.v("MercadoLivre", "PaymentMethodsList Name: " + paymentMethodsList.get(i).getName());
                    }

                    mAdapter = new PaymentMethodsAdapter(PaymentMethodsActivity.this, getApplicationContext(), paymentMethodsList);
                    recyclerView.setAdapter(mAdapter);

                    ((PaymentMethodsAdapter) mAdapter).setOnItemClickListener(new PaymentMethodsAdapter.MyClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            Log.v("MercadoLivre", "Clicou PaymentMethodsList Name: " + paymentMethodsList.get(position).getPayment_type_id() + paymentMethodsList.get(position).getName());
                            Intent intent = new Intent(PaymentMethodsActivity.this, CardIssuersActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(CardIssuersActivity.PAYMENT_TYPE_ID, paymentMethodsList.get(position).getId());
                            bundle.putSerializable(CardIssuersActivity.VALUE_PAYMENT, valuePayment);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ArrayList<PaymentMethods>> call, Throwable t) {
            }
        });
    }
}