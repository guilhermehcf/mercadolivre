package br.com.mercadolivreteste.ui.paymentMethods;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import br.com.mercadolivreteste.R;
import br.com.mercadolivreteste.model.PaymentMethods;

public class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.ViewHolder> {

    private ArrayList<PaymentMethods> mDataSet;
    private Context mContext;
    private Activity mActivity;
    private static PaymentMethodsAdapter.MyClickListener myClickListener;

    public PaymentMethodsAdapter(Activity activity, Context context, ArrayList<PaymentMethods> list) {
        mActivity = activity;
        mDataSet = list;
        mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTextViewNome;
        public ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mTextViewNome = v.findViewById(R.id.textViewNome);
            mImageView = v.findViewById(R.id.imageView);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(PaymentMethodsAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public PaymentMethodsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_payment_methods, parent, false);
        PaymentMethodsAdapter.ViewHolder vh = new PaymentMethodsAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(PaymentMethodsAdapter.ViewHolder holder, final int position) {
        if (!mDataSet.isEmpty()) {

            holder.mTextViewNome.setText(mDataSet.get(position).getName());

            Glide.with(mContext)
                    .load(mDataSet.get(position).getThumbnail())
                    .into(holder.mImageView);
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}