package br.com.mercadolivreteste.ui.installments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import br.com.mercadolivreteste.BuildConfig;
import br.com.mercadolivreteste.MainActivity;
import br.com.mercadolivreteste.R;
import br.com.mercadolivreteste.model.Installments;
import br.com.mercadolivreteste.rest.APIClient;
import br.com.mercadolivreteste.rest.ApiClientInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstallmentsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public static String CARD_ISSUE_ID = "CARD_ISSUE_ID";
    public static String PAYMENT_TYPE_ID = "PAYMENT_TYPE_ID";
    public static String VALUE_PAYMENT = "VALUE_PAYMENT";

    private ApiClientInterface apiService = APIClient.getClient().create(ApiClientInterface.class);
    private ArrayList<Installments> installmentsList = new ArrayList<Installments>();

    private long mIssuerId = 0;
    private String mPaymentMethodId = "";
    private long valuePayment = 0;

    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_issuers);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            valuePayment = extras.getLong(VALUE_PAYMENT);
            mPaymentMethodId = extras.getString(PAYMENT_TYPE_ID);
            mIssuerId = Long.parseLong(extras.getString(CARD_ISSUE_ID));
            if (mPaymentMethodId != null && mIssuerId != 0 && valuePayment > 0) {
                loadInstallments(mPaymentMethodId, valuePayment, mIssuerId);
            }
        }
    }

    private void loadInstallments(String paymentMethodId, double amount, long issuerId) {

        Call<ArrayList<Installments>> call = apiService.getInstallments(BuildConfig.PUBLIC_KEY, paymentMethodId, amount, issuerId);

        call.enqueue(new Callback<ArrayList<Installments>>() {
            @Override
            public void onResponse(Call<ArrayList<Installments>> call, Response<ArrayList<Installments>> response) {
                installmentsList = response.body();
                if (installmentsList != null && installmentsList.size() > 0) {
                    for (int i = 0; i < installmentsList.size(); i++) {
                        for (int y = 0; y < installmentsList.get(i).getPayerCosts().size(); y++) {
                            Log.v("MercadoLivre", "InstallmentsList Message: " + installmentsList.get(i).getPayerCosts().get(y).getRecommended_message());
                        }
                        mAdapter = new InstallmentsAdapter(InstallmentsActivity.this, getApplicationContext(), installmentsList.get(0).getPayerCosts());
                        recyclerView.setAdapter(mAdapter);

                        ((InstallmentsAdapter) mAdapter).setOnItemClickListener(new InstallmentsAdapter.MyClickListener() {
                            @Override
                            public void onItemClick(int position, View v) {
                                Intent intent = new Intent(InstallmentsActivity.this, MainActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(MainActivity.CARD_ISSUE_ID, mIssuerId);
                                bundle.putSerializable(MainActivity.PAYMENT_TYPE_ID, mPaymentMethodId);
                                bundle.putSerializable(MainActivity.VALUE_PAYMENT, valuePayment);
                                bundle.putSerializable(MainActivity.MESSAGE, installmentsList.get(0).getPayerCosts().get(position).getRecommended_message());
                                intent.putExtras(bundle);
                                startActivity(intent);
                                finish();
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Installments>> call, Throwable t) {
            }
        });
    }
}
