package br.com.mercadolivreteste.ui.cardIssuers;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import br.com.mercadolivreteste.R;
import br.com.mercadolivreteste.model.CardIssuers;

public class CardIssuersAdapter extends RecyclerView.Adapter<CardIssuersAdapter.ViewHolder> {

    private ArrayList<CardIssuers> mDataSet;
    private Context mContext;
    private Activity mActivity;
    private static CardIssuersAdapter.MyClickListener myClickListener;

    public CardIssuersAdapter(Activity activity, Context context, ArrayList<CardIssuers> list) {
        mActivity = activity;
        mDataSet = list;
        mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTextViewNome;
        public ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mTextViewNome = v.findViewById(R.id.textViewNome);
            mImageView = v.findViewById(R.id.imageView);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(CardIssuersAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public CardIssuersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_card_issues, parent, false);
        CardIssuersAdapter.ViewHolder vh = new CardIssuersAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(CardIssuersAdapter.ViewHolder holder, final int position) {
        if (!mDataSet.isEmpty()) {

            holder.mTextViewNome.setText(mDataSet.get(position).getName());

            Glide.with(mContext)
                    .load(mDataSet.get(position).getThumbnail())
                    .into(holder.mImageView);
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}