package br.com.mercadolivreteste.ui.cardIssuers;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

import br.com.mercadolivreteste.BuildConfig;
import br.com.mercadolivreteste.R;
import br.com.mercadolivreteste.model.CardIssuers;
import br.com.mercadolivreteste.rest.APIClient;
import br.com.mercadolivreteste.rest.ApiClientInterface;
import br.com.mercadolivreteste.ui.installments.InstallmentsActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardIssuersActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    public static String PAYMENT_TYPE_ID = "PAYMENT_TYPE_ID";
    public static String VALUE_PAYMENT = "VALUE_PAYMENT";

    private ApiClientInterface apiService = APIClient.getClient().create(ApiClientInterface.class);
    private ArrayList<CardIssuers> cardIssuersList = new ArrayList<CardIssuers>();
    private String mPaymentMethodId = "";
    private long valuePayment = 0;

    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_issuers);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            valuePayment = extras.getLong(VALUE_PAYMENT);
            mPaymentMethodId = extras.getString(PAYMENT_TYPE_ID);
            if (mPaymentMethodId != null) {
                loadCardIssuers(mPaymentMethodId);
            }
        }
    }

    private void loadCardIssuers(String paymentMethodId) {

        Call<ArrayList<CardIssuers>> call = apiService.getCardIssuers(BuildConfig.PUBLIC_KEY, paymentMethodId);

        call.enqueue(new Callback<ArrayList<CardIssuers>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<CardIssuers>> call, @NonNull Response<ArrayList<CardIssuers>> response) {
                cardIssuersList = response.body();
                if (cardIssuersList != null && cardIssuersList.size() > 0) {
                    for (int i = 0; i < cardIssuersList.size(); i++) {
                        Log.v("MercadoLivre", "CardIssuersList Name: " + cardIssuersList.get(i).getName());
                    }
                    mAdapter = new CardIssuersAdapter(CardIssuersActivity.this, getApplicationContext(), cardIssuersList);
                    recyclerView.setAdapter(mAdapter);

                    ((CardIssuersAdapter) mAdapter).setOnItemClickListener(new CardIssuersAdapter.MyClickListener() {
                        @Override
                        public void onItemClick(int position, View v) {
                            Log.v("MercadoLivre", "Clicou PaymentMethodsList Name: " + cardIssuersList.get(position).getName());

                            Intent intent = new Intent(CardIssuersActivity.this, InstallmentsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(InstallmentsActivity.CARD_ISSUE_ID, cardIssuersList.get(position).getId());
                            bundle.putSerializable(InstallmentsActivity.PAYMENT_TYPE_ID, mPaymentMethodId);
                            bundle.putSerializable(InstallmentsActivity.VALUE_PAYMENT, valuePayment);
                            intent.putExtras(bundle);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CardIssuers>> call, Throwable t) {
            }
        });
    }
}