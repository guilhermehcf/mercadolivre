package br.com.mercadolivreteste.ui.installments;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.mercadolivreteste.R;
import br.com.mercadolivreteste.model.PayerCosts;

public class InstallmentsAdapter extends RecyclerView.Adapter<InstallmentsAdapter.ViewHolder> {

    private ArrayList<PayerCosts> mDataSet;
    private Context mContext;
    private Activity mActivity;
    private static InstallmentsAdapter.MyClickListener myClickListener;

    public InstallmentsAdapter(Activity activity, Context context, ArrayList<PayerCosts> list) {
        mActivity = activity;
        mDataSet = list;
        mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTextViewMessage;

        public ViewHolder(View v) {
            super(v);
            mTextViewMessage = v.findViewById(R.id.textView);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            myClickListener.onItemClick(getAdapterPosition(), v);
        }
    }

    public void setOnItemClickListener(InstallmentsAdapter.MyClickListener myClickListener) {
        this.myClickListener = myClickListener;
    }

    @Override
    public InstallmentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_installments, parent, false);
        InstallmentsAdapter.ViewHolder vh = new InstallmentsAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(InstallmentsAdapter.ViewHolder holder, final int position) {
        if (!mDataSet.isEmpty()) {

            holder.mTextViewMessage.setText(mDataSet.get(position).getRecommended_message());
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }
}